People asked for my dotfiles. So here they are. They are designed for my usage,
so you will need to modify some files if you decide to copy anything, such as
my name. I will not make them more generic. Keep in mind that there are some
quirks that I've dealt with and non-standard configurations.

**WARNING: Running the installer WILL REPLACE YOUR FILES!**

Some files are Jinja2 templates. This is because of customizations between
environments.

### Requirements to run the installer
- python 3.4 (due to RHEL 7)
- git

### Python libraries
- Jinja2 

### Optional programs
`colordiff source-highlight`

Please note that this repository contains configurations copied from other sources.

The history for this repo was reset upon making public.
