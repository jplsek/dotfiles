# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

# Prompt customization

txtgrn='\e[0;32m' # Green
txtylw='\e[0;33m' # Yellow
txtrst='\e[0m'    # Text Reset

export HISTSIZE=10000

# Source aliases
if [ -f ~/.aliases ]; then
    . ~/.aliases
fi

# Git prompt
export PSSTART="\[$txtgrn\]\u\[$txtrst\]@\[$txtgrn\]\h\[$txtrst\]:\[$txtylw\]\w\[$txtrst\]"
export PSEND=" "
export PS1=$PSSTART$PSEND

GIT_PROMPT_START=$PSSTART
GIT_PROMPT_END=$PSEND
GIT_PROMPT_ONLY_IN_REPO=1
source ~/.bash-git-prompt/gitprompt.sh

# added by travis gem
[ -f /home/{{ user }}/.travis/travis.sh ] && source /home/{{ user }}/.travis/travis.sh
