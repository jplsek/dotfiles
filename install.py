#!/usr/bin/env python3
# This script is meant to be somewhat quick-and-dirty
import os
import platform
from argparse import ArgumentParser
from getpass import getuser
from jinja2 import Template
from shutil import rmtree
from subprocess import check_call

parser = ArgumentParser()
# Home vs Work email
parser.add_argument('-e', required=True, help='Email address')
# Home vs Work editor (nvim vs vim)
parser.add_argument('-v', required=True, help='Editor')
args = parser.parse_args()

check_call(['git', 'submodule', 'update', '--init', '--recursive'])

template_args = dict(email=args.e, editor=args.v, user=getuser())


def mkdir(directory):
    directory = os.path.expanduser(directory)
    if not os.path.exists(directory):
        os.makedirs(directory)


def link(file_name, from_location='~/dotfiles/', to_location='~/'):
    mkdir(to_location)
    to_location = os.path.expanduser(to_location + file_name)
    from_location = os.path.expanduser(from_location + file_name)
    if os.path.exists(to_location) or os.path.lexists(to_location):
        os.remove(to_location)
    print('Symlink: {} to {}'.format(from_location, to_location))
    os.symlink(from_location, to_location)


def render(file_name):
    with open(file_name) as f:
        t = Template(f.read())
        rendered = t.render(template_args)
        with open('rendered/{}'.format(file_name), 'w') as f:
            f.write(rendered)
        link(file_name, from_location='~/dotfiles/rendered/')


def clone(url, to):
    to = os.path.expanduser(to)
    if os.path.exists(to):
        rmtree(to)
    check_call(['git', 'clone', url, to, '--depth=1'])


mkdir('rendered')
render('.gitconfig')
render('.aliases')
render('.bashrc')
render('.zshrc')

link('.ctags')
link('.oh-my-zsh')
link('.screenrc')
link('.tmux.conf')
link('.profile')
link('.p10k.zsh')
link('ideavimrc', to_location='~/.config/ideavim/')

if 'Linux' in platform.platform():
    link('.dircolors')
    link('.dircolors_256')

link('settings.json', to_location='~/.config/Code/User/')

if 'Linux' in platform.platform():
    link('99-fira-code-emoji.conf', to_location='~/.config/fontconfig/conf.d/')
    check_call(['fc-cache'])

clone('https://github.com/zsh-users/zsh-syntax-highlighting.git',
      '~/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting')
clone('https://github.com/zsh-users/zsh-autosuggestions.git',
      '~/.oh-my-zsh/custom/plugins/zsh-autosuggestions')
clone('https://github.com/romkatv/powerlevel10k.git',
      '~/.oh-my-zsh/custom/themes/powerlevel10k')
clone('https://github.com/magicmonty/bash-git-prompt.git',
      '~/.bash-git-prompt')
